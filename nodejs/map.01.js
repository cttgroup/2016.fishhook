import L from 'leaflet'
import LeafletDraw from 'leaflet-draw'


L.mapbox.accessToken = 'pk.eyJ1Ijoic2VyZ2V5c3luZXJneSIsImEiOiJjaWd3Zm9qejUwMDlydnFrcmloeTBwOW9vIn0.4JdGQv9pO-z4ncyf-DO4Ow';

let lat = 55.7522222
let lng = 37.6155556
let latlng = [55.7522222, 37.6155556]

var map = L.mapbox.map('map', 'mapbox.streets', {
        'zoomControl': false
    })
    .setView([lat, lng], 3);

map.addControl(L.control.zoom({position: 'topright'}))

var coordinates = document.getElementById('coordinates');

var marker = L.marker([0, 0], {
    icon: L.mapbox.marker.icon({
      'marker-color': '#f86767'
    }),
    draggable: true
})//.addTo(map);

// every time the marker is dragged, update the coordinates container
marker.on('dragend', ondragend);
// Set the initial marker coordinate on load.
ondragend();
function ondragend() {
    var m = marker.getLatLng();
    coordinates.innerHTML = 'Latitude: ' + m.lat + '<br />Longitude: ' + m.lng;
}



/* Initialise the draw control and pass it the FeatureGroup of editable layers
var drawControl = new LeafletDraw({
    edit: {
        featureGroup: drawnItems
    }
});
//map.addControl(drawControl);
*/

