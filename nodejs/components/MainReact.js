'use strict'
import React from 'react'
import ReactDOM from 'react-dom'
//import {Map} from './map'
//import {MapReact} from './mapreact'
import {Sidebar} from './Sidebar'


// Вывод карты обычным для leaflet способом
//import {oldmap, oldlayer} from "./oldmap";
//oldlayer.addTo(oldmap)


var Header = React.createClass({
    render: function() {
        return (
            <div className='header'>
                <h1 className="item"> Карта </h1>
            </div>
        )
    },
})


var MainReact = React.createClass({
    updateSidebarData: function() {
        //console.log('main update')
        this.refs.theSidebar.updateData()
    },
    render: function() {
        return (<div>
            <Sidebar ref="theSidebar" latLng={__latLng} />
        </div>)
    },
})
            /*
            <Header />
            <MapReact className='mapreact' />
            */


module.exports = MainReact


