# -*- coding: utf-8 -*-
import os
import subprocess
import json

import requests
from osgeo import ogr
from flask import Flask
from flask_cors import CORS
from flask import render_template


app = Flask(__name__)
CORS(app)
app.debug = True
app.use_reloader = True


def make_thumb(thumb):
    filename = '/volumecache/' + thumb
    if not os.path.exists(filename):
        args = ["wget", thumb]
        proc = subprocess.Popen(
            args, cwd='/volumecache/thumbs', stdout=subprocess.PIPE)


@app.route('/')
def index():
    return 'REST-API server for landsat-util'


@app.route('/thumb/<sceneID>')
def thumb(sceneID):
    return render_template('thumb.html', sceneID=sceneID)


def get_shape(_row, _path):
    shapefile = "./wrs2_descending/wrs2_descending.shp"
    driver = ogr.GetDriverByName("ESRI Shapefile")
    dataSource = driver.Open(shapefile, 0)
    layer = dataSource.GetLayer()

    lonLatShape = []
    for feature in layer:
        geom = feature.GetGeometryRef()
        row = int(feature.GetField("ROW"))
        path = int(feature.GetField("PATH"))
        if _row == row and _path == path:
            lonLatShape = json.loads(geom.ExportToJson())["coordinates"][0]

    latLonShape = []
    for point in lonLatShape:
        latLonShape.append([point[1], point[0]])

    return latLonShape


@app.route('/lat/<lat>/lon/<lon>')
# Значения по умолчанию - координаты ширины и долготы Москвы
def get_landsat_data(lat=55.7522222, lon=37.6155556):
    # Проверяем наличие каталога volumecache, создаём в случае отсутствия
    """
    directory = '/volumecache'
    if not os.path.exists(directory):
        os.makedirs(directory)
    """
    # Проверяем наличие каталога thumbs, создаём в случае отсутствия
    directory = '/volumecache/thumbs'
    if not os.path.exists(directory):
        os.makedirs(directory)

    # Производим проверку типов переданной широты и долготы
    if isinstance(float(lat), float) and isinstance(float(lon), float):
        # Получаем путь к файлу
        filename = '/volumecache/' + str(lat) + '_' + str(lon)

        # Механизм кэширования - исключительно для отладки
        # Если файл отсутствует, производим обработку:
        if not os.path.exists(filename):
            # Задаём массив параметров для утилиты landsat
            args = [
                "landsat", "search", "--lat", str(lat),
                "--lon", str(lon), "--json"]

            # Запускаем утилиту landsat во внешнем процессе
            proc = subprocess.Popen(args, stdout=subprocess.PIPE)

            # Подключаемся в stdout запущенного процесса
            # и получаем выводимые значения;
            # полученные значения переводим в формат json
            json_data = json.loads(proc.communicate()[0])

            # Создаём указатель на словарь results в полученных данных
            results = json_data['results']
            i = 0

            # Добавляем к полученным данным координаты векторного
            # контура снимка
            while i < len(results):
                row = int(results[i]['row'])
                path = int(results[i]['path'])
                shape = get_shape(row, path)
                results[i]['shape'] = shape
                # Скачиваем превью изображений
                make_thumb(results[i]['thumbnail'])
                i += 1

            # Записываем все полученные и обработанные данные в файл
            with open(filename, 'w') as f:
                    f.write(json.dumps(json_data))

        # Загружаем данные из файла в формате json
        with open(filename, 'r') as f:
            json_data = json.loads(f.read())

        results = json_data['results']
        i = 0
        while i < len(results):
                url = 'http://localhost:8000/images/'+results[i]['sceneID']
                results[i]['state'] = url
                i += 1
        return json.dumps(json_data)

    return "Wrong request: " + lat + "; " + lon


@app.route('/download/<sceneID>')
# Значения по умолчанию - координаты ширины и долготы Москвы
def download(sceneID):
    # Проверяем наличие каталога images, создаём в случае отсутствия
    directory = '/workdir/images/'
    if not os.path.exists(directory):
        os.makedirs(directory)

    # Производим проверку на наличие аргумента sceneID
    if sceneID:
        # Получаем путь к файлу
        # Если файл отсутствует, производим обработку:
        if False:
            # Задаём массив параметров для утилиты landsat
            args = [
                "landsat",
                "download",
                "-d",
                "images/" + sceneID,
                sceneID
            ]

            # Запускаем утилиту landsat во внешнем процессе
            proc = subprocess.Popen(args, stdout=subprocess.PIPE)

            # Подключаемся в stdout запущенного процесса и получаем
            # выводимые значения;
            # полученные значения переводим в формат json
            # json_data = json.loads(proc.communicate()[0])

            results = (proc.communicate()[0])
            return results

        return 'downloadingFinished'

    return "Wrong request: " + sceneID


if __name__ == '__main__':
    # Адрес порта устанавливаем как PORT если определено,
    # иначе устанавливаем по значению аргумента
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
