# -*- coding: utf-8 -*-
from django.conf.urls import include, url
from django.contrib import admin

from zcore.views import *
import app.settings as settings


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
]

urlpatterns += [
    #url(r'^static/(?P<path>.*)$', 'django.views.static.serve',{'document_root': settings.URL_STATIC_ROOT}),

    url(r'^images/(?P<sceneID>.*)$', check_images),
    url(r'^imagesgo/(?P<sceneID>.*)$', imagesgo),
    url(r'^images$', ImagesList.as_view()),
    #url(r'^lat/(?P<lat>\d+\.\d)/lon/(?P<lon>\d+\.\d)$', get_landsat_data),
    url(r'^lat/(?P<lat>[0-9,\.]+)/lon/(?P<lon>[0-9,\.]+)$', get_landsat_data),
]


