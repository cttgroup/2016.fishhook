# -*- coding: utf-8 -*-
import json
import time
import requests

from django.core import serializers
from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.http import HttpResponseRedirect

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import generics

from .models import Image
from .serializers import ImageSerializer


class ImagesList(generics.ListCreateAPIView):
    queryset = Image.objects.all()
    serializer_class = ImageSerializer

class ImagesDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Image.objects.all()
    serializer_class = ImageSerializer

def check_images(request, sceneID):
    response = HttpResponse()
    try:
        image = Image.objects.get(title=sceneID)
    except:
        if sceneID:
            image = Image()
            image.title = sceneID
            image.state = 'new'
            image.save()
        else:
            return HttpResponseRedirect('/images?format=json')
    images = Image.objects.filter(title=sceneID)
    response.write(serializers.serialize('json', images))
    #time.sleep(10)
    return response

def imagesgo(request, sceneID):
    response = HttpResponse()
    try:
        image = Image.objects.get(title=sceneID)
        image.state = 'complete'
        image.save()
    except:
        return HttpResponseRedirect('/images?format=json')
    images = Image.objects.filter(title=sceneID)
    response.write(serializers.serialize('json', images))
    return response

def get_landsat_data(request, lat, lon):
    response = HttpResponse()
    # Производим проверку типов переданной широты и долготы
    if isinstance(float(lat), float) and isinstance(float(lon), float):
        url = 'http://localhost:5000/lat/' + lat + '/lon/' + lon
        r = requests.get(url)
        response.write(str(r.text))
    else:
        response.write("Неправильный формат широты и долготы")

    return response
