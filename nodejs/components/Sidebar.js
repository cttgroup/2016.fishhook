import React from 'react'
import http from 'http'
import { Popover, Button, Collapse, Well } from 'react-bootstrap';

import Spinner from './Spinner'
import Cement from './Cement'


var func = ''
const state = ['zero', 'awating', 'new', 'complete']

var Sidebar = React.createClass({
    updateData: function() {
        this.setState({statusBar: 'Получаю данные...', complete: [], uncomplete: []})
        //this.setState({tip: 'Получаю данные...'+Math.random()})
        //let url = 'http://' + window.location.hostname + ':23008/lat/55.7522222/lon/37.6155556'
        let url = 'http://' + window.location.hostname + ':23008/lat/'+__latLng.lat+'/lon/'+__latLng.lng

        console.log("URL",url)
        Cement.loadXHRData(url, function(data) {
            let complete = []
            let uncomplete = []
            let i = 0
            while (i < data.results.length) {
                let el = data.results[i]
                url = 'http://' + window.location.hostname + ':28003/images/' + el.sceneID
                Cement.loadXHRData(url, function(data) {
                    let state = data[0].fields.state
                    if (state == 'complete') {
                        complete.push(el)
                    } else {
                        uncomplete.push(el)
                    }
                    this.setState({complete: complete, uncomplete: uncomplete, open: true})
                }.bind(this))
                i++
            }
        }.bind(this))
    },
    getInitialState: function() {
        return {
            statusBar: 'Выберите точку на карте',
            complete: [],
            uncomplete: [],
            open: false,
        }
    },
    render: function() {
        let rows = []
            //console.log("UN", this.state.uncomplete)
        if (typeof this.state.uncomplete !== 'undefined') {
            rows.push(<ImagesFound
                data={this.state.uncomplete}
                complete={false}
                _update={this.updateData}
                open={true}
                tip={'Новые снимки: ' + this.state.uncomplete.length}
                statusBar={this.state.statusBar}
            />)
        }
        if (typeof this.state.complete !== 'undefined') {
            rows.push(<ImagesFound
                data={this.state.complete}
                complete={true}
                _update={this.updateData}
                open={false}
                tip={'Обработанные снимки: ' + this.state.complete.length}
                statusBar={this.state.statusBar}
            />)
        }


        return(
            <div className='sidebar'>
                {rows}
            </div>
        )
    },
})


var ImagesFound = React.createClass({
    getInitialState: function() {
        return {
            open: this.props.open,
        }
    },
    render: function() {
        let elements = this.props.data
        let rows = []
        let length = 0
        let tip = ''

        //if (typeof (elements = this.props.data) !== '') {
        if (elements.length > 0) {
            elements.forEach(function(el, key) {
                rows.push(<Thumb
                    key={key}
                    date={el.date}
                    sceneID={el.sceneID}
                    thumbnail={el.thumbnail}
                    complete={this.props.complete}
                    _update={this.props._update}
                />)
            }.bind(this))
            if (elements.length > 0) {
                tip = elements.length
            }

            return(
                <div>
                    <Button onClick={ ()=> this.setState({ open: !this.state.open })}>
                        {this.props.tip}
                    </Button>
                    <Collapse in={this.state.open}>
                        <Well>
                            {rows}
                        </Well>
                    </Collapse>
                </div>
            )
        } else if (!this.props.complete) {
            //console.log(this.props.complete)
            return (
                <div>
                    <Popover placement="right" positionLeft={200} positionTop={50} title="">
                        {this.props.statusBar}
                    </Popover>
                </div>)
        } else {
            return (<div></div>)
        }
    },
})


/*
var ThumbUncomplete = React.createClass({
    render: function() {
        const style = {
          backgroundImage: 'url(' + this.props.thumbnail + ')',
        }

        return(
            <div className='thumb' style={style}>
                <LoadingButton sceneID={this.props.sceneID} />
            </div>
        )
    },
                //<ProgressIndicator />
                //<img src={this.props.thumbnail} className="thumbnail"/>
})
*/


var Thumb = React.createClass({
    render: function() {
        const style = {
          backgroundImage: 'url(' + this.props.thumbnail + ')',
        }
        let rows = []
        if (!this.props.complete) {
            rows.push(
                <LoadingButton sceneID={this.props.sceneID} _update={this.props._update} />
            )
        } else {
            rows.push(
                    <a
                    href={'/volumecache/thumbs/'+this.props.sceneID + '.jpg'}
                    target={'_blank'}
                    >Обработанное изображение</a>
            )
        }

        return(
            <div className='thumb' style={style}>
                {rows}
            </div>
        )
    },
})


const LoadingButton = React.createClass({
    getInitialState() {
        return {
            isLoading: false
        }
    },
    render() {
        let isLoading = this.state.isLoading;
        return (
            <Button
                bsStyle="primary"
                disabled={isLoading}
                onClick={!isLoading ? this.handleClick : null}>
                {isLoading ? 'Выполняется...' : 'Обработать'}
            </Button>
        )
    },
    handleClick() {
        this.setState({isLoading: true})
        let url = 'http://' + window.location.hostname + ':28003/imagesgo/' + this.props.sceneID
        setTimeout(() => {
            Cement.loadXHRData(url, function(data) {
                if (typeof this.state.isLoading !== 'undefined') {
                    this.setState({isLoading: false, data: data})
                } else {
                    this.setState({data: data})
                }
                if (typeof (func = this.props._update) === 'function') { func() }
            }.bind(this))
            // Completed of async action, set loading state back
            //this.setState({isLoading: false});
        }, 1000)
    }
})


/*
var ProgressIndicator = React.createClass({
    render: function() {
        return(
            <LoadingButton>Default</LoadingButton>
        )
    },
            //<div className="progress-indicator"> 99 % </div>
})


const PI = React.createClass({
  render: function() {
    const style = {
      height: 50,
      width: 50
      //backgroundColor: 'black'
    };
    return (
      <div style={style} className='progress-indicator'>
        <Spinner />
      </div>
    );
  }
});
*/

export {Sidebar}
