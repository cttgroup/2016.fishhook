import Leaflet from 'leaflet'
import LeafletDraw from 'leaflet-draw'
import React from 'react'
import ReactDOM from 'react-dom'

import MainReact from './components/MainReact'
import Cement from './components/Cement'

require("leaflet/dist/leaflet.css")
require("leaflet-draw/dist/leaflet.draw.css")


//let localReact = new {MainReact}
var MainReactElement = <MainReact />
var SidebarInstance = ReactDOM.render(MainReactElement, mainMount)

//L.Icon.Default.imagePath = 'node_modules/leaflet/dist/images/'
L.Icon.Default.imagePath = 'static/leaflet_images'
//L.Icon.Default.imagePath = 'node_modules/leaflet/dist/images'
let accessToken = 'pk.eyJ1Ijoic2VyZ2V5c3luZXJneSIsImEiOiJjaWd3Zm9qejUwMDlydnFrcmloeTBwOW9vIn0.4JdGQv9pO-z4ncyf-DO4Ow';
let mapboxUrl = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token='+accessToken
let id = 'sergeysynergy.cigwfojr8008nstlykqyvldtv'
let mbAttr = ''

let lat = 55.7522222
let lng = 37.6155556
let latlng = [55.7522222, 37.6155556]
let zdenver = [39.74, -104.99]
let zmoscow = [55.752222,37.6155556]


function drawShapes(results) {
    results.forEach(function(element){
        //console.log("SHape",element.shape)
        L.polyline(element.shape, {color: '#333'}).addTo(map)
    })
}


// Определяем основные слои-покрытия карты
let grayscale  = L.tileLayer(mapboxUrl, {
    id: 'mapbox.light', 
    attribution: ''
})
let streets  = L.tileLayer(mapboxUrl, {
    id: 'mapbox.streets',
    attribution: ''
})
let defaultTile = L.tileLayer(mapboxUrl, {
    id: id,
    maxZoom: 18,
    attribution: ''
})

// Определяем накладные слои
var cities = new L.LayerGroup()
L.marker([39.61, -105.02]).bindPopup('This is Littleton, CO.').addTo(cities)
L.marker([39.74, -104.99]).bindPopup('This is Denver, CO.').addTo(cities)
L.marker([39.73, -104.8]).bindPopup('This is Aurora, CO.').addTo(cities)
L.marker([39.77, -105.23]).bindPopup('This is Golden, CO.').addTo(cities)

var featureGroup = L.featureGroup()

// Определяем карту
var map = L.map('mapMount', {
    center: zmoscow,
    zoom: 4,
    zoomControl: false,
    layers: [defaultTile, featureGroup]
})

var baseLayers = {
    "Grayscale": grayscale,
    "Streets": streets,
    "Основной": defaultTile
}

var overlays = {
    "Cities": cities,
    "Features": featureGroup
}

// Определяем контрол зума
L.control.zoom({position: 'topright'}).addTo(map)

// Определяем контрол слоёв
L.control.layers(baseLayers, overlays, {
    //width: '30px',
    //height: '30px'
})//.addTo(map)


// Определяем свой контрол
let selectBoxControl = L.Control.extend({
    options: {
        position: 'topright' 
    },

    onAdd: function (map) {
        let container = L.DomUtil.create(
            'div', 
            'leaflet-bar leaflet-control leaflet-control-custom'
        )
 
        //container.style.backgroundColor = 'white'
        container.style.width = '30px'
        container.style.height = '30px'
        container.style.backgroundImage = "url(static/images/cat.png)"
        container.style.backgroundSize = "30px 30px"
 
        container.onclick = function(){
            console.log('buttonClicked')
        }

        container.onmouseover = function(){
            console.log('mouseover')
        }

        return container
  },

})

//map.addControl(new selectBoxControl())



var drawControl = new L.Control.Draw({
    position: 'topright',
    draw: {
        polyline: false,
        polygon: false,
        circle: false,
        rectangle: false
        /*
        {
            metric: true,
            //repeatMode: true,
            shapeOptions: {
                color: 'gray',
                dashArray: '5, 10',
                //clickable: false
            }
        },
        */
        //marker: { }
    },
    edit: {
        featureGroup: featureGroup,
        remove: false,
        edit: false
    }
}).addTo(map)


map.on('draw:drawstart', function(e) {
    //featureGroup.clearLayers()
})


let shmarker = L.marker(zdenver)


function getImagesData() {
    SidebarInstance.updateSidebarData()
    //let url = 'http://' + window.location.hostname + ':23008/lat/55.7522222/lon/37.6155556'
    let url = 'http://' + window.location.hostname + ':23008/lat/'+__latLng.lat+'/lon/'+__latLng.lng
    Cement.loadXHRData(url, function(data) {
        drawShapes(data.results)
    }.bind(this))
}


function onMarkerClick(e) {
    //console.log(this.getLatLng())
    __latLng = this.getLatLng()
    ReactDOM.render( <MainReact latLng={__latLng} />, mainMount)
    getImagesData()
}

map.on('draw:created', function (e) {
    //featureGroup.clearLayers()
    var type = e.layerType,
        layer = e.layer;

    let out = ''

    if (type === 'marker') {
        __latLng = layer.getLatLng()
        //layer.bindPopup(__latLng)
        layer.on('click', onMarkerClick)
        getImagesData()
    }

    if (type === 'rectangle') {
        //shmarker.addTo(map)
        //shmarker.bindPopup("Denver")
        out = layer.getLatLngs()
        let bounds = layer.getBounds()
        let lat1 = bounds._northEast.lat
        let lng1 = bounds._southWest.lng
        console.log("bounds",bounds)
        console.log("lat1",lat1)
        console.log("lng1",lng1)
    }

    featureGroup.addLayer(layer);
    //console.log("THIS",map)

    map.eachLayer(function (layer) {
        //layer.bindPopup('Hello');
        //console.log("LAYER",layer)
    });
})




//SidebarInstance.updateSidebarData()
module.exports = map


