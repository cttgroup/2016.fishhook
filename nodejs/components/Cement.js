//import http from 'http'

exports.loadXHRData = function(url, callback) {
    // Инициализируем объект XMLHttpReques, позволяющий отправлять 
    // асинхронные запросы веб-серверу и получать ответ без перезагрузки страницы
    var xhr = new XMLHttpRequest()

    xhr.responseType = 'json'
    xhr.open('GET', url, true)

    // Возвращаем данные, полученные по запросу
    xhr.onreadystatechange = function() {
        if(xhr.readyState == 4) { // Запрос выполнен
            if (xhr.status === 200 || xhr.status === 204) {
                //console.log("DATA",xhr.response)
                if (callback && typeof(callback) === "function") {
                // Выполняем функцию обратного вызова. В качестве параметра передаём 
                // асинхронно полученные данные
                    callback(xhr.response)
                }
            } else {
                console.log('Ошибка: ' + xhr.status)
            }
        }
    }

    // Производим отправку асинхронного запроса
    //console.log('Sending async GET request to:',url)
    xhr.send()
}


