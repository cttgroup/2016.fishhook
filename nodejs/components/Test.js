'use strict'
import React from 'react'
import ReactDOM from 'react-dom'


var Test = React.createClass({
    render: function() {
        let rowClass = 'row3'

        return (
            <div>
                <h2>Test 1</h2>
                <Way />
                <h2>Test 2</h2>
                <Way />
            </div>
        )
    },
})


var Way = React.createClass({
    render: function() {
        return (
            <div className='main-ways'>
                <ZRow id={1} />
                <ZRow id={2} />
                <ZRow id={3} />
                <div className='clearfix'></div>
                <div className='body'>
                    [[66.461557694864, 45.485845610102544], [66.50211936113908, 45.21868871940561], [66.51225540580694, 45.15192829198469], [66.5127, 45.149], [66.54603034338064, 45.181797746308966], [66.58330268231497, 45.21847449766281], [66.85499596973246, 45.48582630417746], [67.87459064523289, 46.48912852411374], [67.90692254614017, 46.52094378240656], [67.9458, 46.5592], [67.94518721292947, 46.56320203281746], [67.3287, 50.5894], [67.2920727202125, 50.54739583030162], [65.96368493420947, 49.024000527340114], [65.9302, 48.9856], [66.39513243555724, 45.92335144676578], [66.461557694864, 45.485845610102544]]
                </div>
                <div className='clearfix'></div>
            </div>
        )
    },
})


var ZRow = React.createClass({
    getInitialState: function(){
        return {
            bgId: this.props.id,
        }
    },
    render: function() {
        let newbgId = (this.state.bgId === 3) ? 1 : this.state.bgId + 1;
        let rowClass = 'row3'

        setTimeout(function () {
            this.setState({bgId: newbgId})
        }.bind(this), 7000)
        
        let imgUrl = '/static/images/0' + this.state.bgId + '.jpg'
        let divStyle = {
            backgroundImage: 'url(' + imgUrl + ')'
        }

        return (
                <div className={rowClass}>
                    <div style={divStyle}>[66.461557694864, 45.485845610102544], [66.50211936113908, 45.21868871940561], [66.51225540580694, 45.15192829198469]</div>
                </div>
        )
    },
})


module.exports = Test


