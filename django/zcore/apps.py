from django.apps import AppConfig


class ZcoreConfig(AppConfig):
    name = 'zcore'
